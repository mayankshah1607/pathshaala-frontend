import React, { Component } from 'react';
import CardView from '../../Components/ListCard/Card';

export default class ListCard extends Component{

    render(){
        const rows = this.props.data.map((obj, i) => {
            return(
                <CardView obj={obj}/>
            );
        });


        return(
            <div>
                {
                    this.props.data.length ?
                    <div>
                    <p style={{
                        'paddingLeft':'24px',
                        'fontSize':'12px'
                    }}>{`${this.props.data.length} item(s) fetched`}</p>
                    {rows}
                    </div> 
                    :
                    <div style={{'color':'#888888',
                                'fontWeight':'700',
                                'textAlign':'center',
                                'fontSize':'18px'
                }}>No results fetched</div>
                }
            </div>
        );
    }
}

