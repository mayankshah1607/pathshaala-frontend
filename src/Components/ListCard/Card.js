import React, { Component } from 'react';
import { Card, Icon } from 'antd';
import './ListCard.css';

const gridStyle = {
    width: '25%',
    textAlign: 'left',
  };

export default class CardView extends Component{
    constructor(){
        super()
        this.state = {}
    }

    render(){
        const obj = this.props.obj;
        const date = new Date(obj.DateAdded);
        const imgUrl = obj.imgUrl;
        return(
            <div id='card'>
                <div className='img' style={{'backgroundImage':`url(${imgUrl})`}}>
                </div>
                <Card
                      title={obj.problem}
                      extra={<a>Show Interest <Icon type="like" /></a>}
                      style={{ width: 800 }}
                >
                <p class='sub'>{obj.goal}</p>
                <p>{obj.description}</p>
                <p class='meta'><Icon type="home" /> {`${obj.block}, ${obj.district},${obj.village}, Maharashtra`}</p>
                <p class='meta'><Icon type="fund" /> Fund Requirements: Rs.{obj.required_fund}</p>
                <p class='meta'><Icon type="calendar" /> {`Date added (dd/mm/yyyy): ${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`}</p>
                </Card>
            </div> 
        );
    }
}