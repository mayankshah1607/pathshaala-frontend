import React from 'react';
import Marketplace from './Pages/Marketplace/Marketplace';
import ProposalBuilder from './Pages/ProposalBuilder/ProposalBuilder';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

require('dotenv').config();

function App() {
  return (
    <div className="App"> 

      <Router>
        <Switch>
          <Route path='/marketplace' component={Marketplace} />
          <Route path='/proposal-builder' component={ProposalBuilder} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
