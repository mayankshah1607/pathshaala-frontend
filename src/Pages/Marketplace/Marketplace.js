import React, { Component } from 'react';
import './Marketplace.css';
import { Menu, Dropdown, Icon, Input, Slider, Button } from 'antd'; 
import ListCard from '../../Components/ListCard/ListCard';


const Search = Input.Search;

var blockCheck = [], districtCheck = [], villageCheck = [];



export default class Marketplace extends Component{
    constructor(){
        super()
        this.state = {
            district: '',
            block: '',
            village: '',
            management: '',
            grade: '',
            medium: '',
            type: '',
            data: [],
            budget_min: 30000,
            budget_max: 500000
        }
    }

    componentDidMount(){
      fetch(`${process.env.REACT_APP_API_URL}/marketplace/get-all-proposals`, {
        method: 'get',
        headers: {'Content-type':'application/json'}
      })
      .then(response => response.json())
      .then(data => {
        if (data.success){
          let dataArr = [];
          dataArr = data.payload;
          this.setState({data: dataArr});
        }
      })
      .catch(err => {
        alert('Unable to fetch data at the moment!')
      })
    }

    changeBudget = (e) => {
        this.setState({budget_min: e[0], budget_max: e[1]})
    }
    changeMinMax = (e) => {
      if(e.target.name === "minInput"){
        this.setState({budget_min: e.target.value});
      } else {
        this.setState({budget_max: e.target.value});
      }
    }

    onReset = () => {
      this.setState({
        district: '',
        block: '',
        village: '',
        management: '',
        grade: '',
        medium: '',
        type: '',

      })
    }

    onSearch = (value) => {
      if (value !== ''){
        fetch(`${process.env.REACT_APP_API_URL}/marketplace/search`,{
          method:'post',
          headers: {'Content-type':'application/json'},
          body: JSON.stringify({searchQuery: value})
        })
        .then(response => response.json())
        .then(data => {
          if (data.success) this.setState({data: data.payload})
        })
        .catch(err => console.log('Error, ', err))
      } else {
        fetch(`${process.env.REACT_APP_API_URL}/marketplace/get-all-proposals`, {
          method: 'get',
          headers: {'Content-type':'application/json'}
        })
        .then(response => response.json())
        .then(data => {
          if (data.success){
            let dataArr = [];
            dataArr = data.payload;
            this.setState({data: dataArr});
          }
        })
        .catch(err => {
          alert('Unable to fetch data at the moment!')
        })
      }

      console.log('check', this.state.data)
      }

      onFilterSelect = (name, e) => {
        if (name === 'district') this.setState({district: e.key})
        if (name === 'block') this.setState({block: e.key})
        if (name==='village') this.setState({village: e.key})
        if(name==='management') this.setState({management: e.key})
        if (name==='grade') this.setState({grade: e.key})
        if (name==='medium') this.setState({medium: e.key})
        if (name==='type') this.setState({type: e.key})
      }

      onFilterSearch = () => {
        const cond = {
          district : !this.state.district.length ? {$exists: true} : this.state.district,
          block : !this.state.block.length ? {$exists: true} : this.state.block,
          village : !this.state.village.length ? {$exists: true} : this.state.village,
          management : !this.state.management.length ? {$exists: true} : this.state.management,
          grade : !this.state.grade.length ? {$exists: true} : this.state.grade,
          medium : !this.state.medium.length ? {$exists: true} : this.state.medium,
          type : !this.state.type.length ? {$exists: true} : this.state.type,
          required_fund: {$gte: this.state.budget_min, $lte: this.state.budget_max}
        }
        fetch(`${process.env.REACT_APP_API_URL}/marketplace/filter-search`,{
          method: 'post',
          headers: {'Content-type':'application/json'},
          body: JSON.stringify({cond:cond})
        })
        .then(response => response.json())
        .then(data => {
          if (data.success){
            this.setState({data: data.payload})
          }
          else{
            alert('Oops! Could not fetch your data')
          }
        })
        .then(err => {
          console.log('Something went wrong. Try again later')
        })
      }


    render(){
        const medium = (
            <Menu onClick={this.onFilterSelect.bind(this, "medium")}>
              <Menu.Item key='Marathi'>Marathi</Menu.Item>
              <Menu.Item key='Hindi'>Hindi</Menu.Item>
              <Menu.Item key='English'>English</Menu.Item>
            </Menu>
          );
          
          const block = (
            
            <Menu onClick={this.onFilterSelect.bind(this, "block")}>
              {
                (function(){
                  blockCheck =[];
                })()
              }
              { 
                this.state.data.map((obj, i) => {
                  if (!(blockCheck.indexOf(obj.block) > -1)){
                    blockCheck.push(obj.block);
                    return (
                      <Menu.Item key={obj.block}>{obj.block}</Menu.Item>
                    );
                  }
                  return null;
              })
              }
            </Menu>
          );

          const village = (
            <Menu onClick={this.onFilterSelect.bind(this, "village")}>
              {
                (function(){
                  villageCheck =[];
                })()
              }
              {
                this.state.data.map((obj, i) => {
                  if (!(villageCheck.indexOf(obj.village) > -1)){
                    villageCheck.push(obj.village);
                    return (
                      <Menu.Item key={obj.village}>{obj.village}</Menu.Item>
                    );
                  }
                  return null;
              })
              }
            </Menu>
          );
            
          const district = (
            <Menu onClick={this.onFilterSelect.bind(this, "district")}>
              {
                (function(){
                  districtCheck =[];
                })()
              }
              {
                this.state.data.map((obj, i) => {
                  if (!(districtCheck.indexOf(obj.district) > -1)){
                    districtCheck.push(obj.district);
                    return (
                      <Menu.Item key={obj.district}>{obj.district}</Menu.Item>
                    );
                  }
                  return null;
              })
              }
            </Menu>
          );

          const grade = (
            <Menu onClick={this.onFilterSelect.bind(this, "grade")}>
              <Menu.Item key='Primary' >Primary School</Menu.Item>
              <Menu.Item key='Middle'>Middle School</Menu.Item>
              <Menu.Item key='High'>High School</Menu.Item>
            </Menu>
          );

          const management = (
            <Menu onClick={this.onFilterSelect.bind(this, "management")}>
              <Menu.Item key='Government run'>Government Run</Menu.Item>
              <Menu.Item key='Private school'>Private School</Menu.Item>
            </Menu>
          );

          const type = (
            <Menu onClick={this.onFilterSelect.bind(this, "type")}>
              <Menu.Item key='Boys only'>Boys only </Menu.Item>
              <Menu.Item key='Girls only'>Girls Only</Menu.Item>
              <Menu.Item key='Co-ed'>Co-ed</Menu.Item>
            </Menu>
          );
        return(
        <div id='marketplace'>
            <div id='filter-holder'>
                <Search
                placeholder="Search for projects"
                enterButton="Search"
                onSearch={this.onSearch}
                />
                <br/><br/>
                <p style={{'color':'#EF5A20'}}>Enter your budget</p>
                <div id='budget-holder'>
                    <Input type='number' placeholder={'Min'} style={{'width':'100px'}} value={this.state.budget_min} name="minInput" onChange={this.changeMinMax}/>
                    <Slider onChange={this.changeBudget} range min={10000} max={1000000} defaultValue={[30000, 500000]} value={[this.state.budget_min, this.state.budget_max]} style={{'width':'420px'}}/>
                    <Input type='number' placeholder={'Max'} style={{'width':'100px'}} value={this.state.budget_max} name="maxInput" onChange={this.changeMinMax}/>
                </div>
                <br/>
                <Dropdown overlay={district} >
                    <a className="ant-dropdown-link" href=""> {this.state.district.length ? this.state.district : 'Select district'} <Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={block} >
                    <a className="ant-dropdown-link" href=""> {this.state.block.length ? this.state.block : 'Select block'} <Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={village} >
                    <a className="ant-dropdown-link" href=""> {this.state.village.length ? this.state.village : 'Select village'} <Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={management} >
                    <a className="ant-dropdown-link" href=""> {this.state.management.length ? this.state.management : 'Select management type'} <Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={grade} >
                    <a className="ant-dropdown-link" href="">{this.state.grade.length ? this.state.grade : 'Select grade'}<Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={medium} >
                    <a className="ant-dropdown-link" href=""> {this.state.medium.length ? this.state.medium : 'Select medium'} <Icon type="down" /></a>
                </Dropdown>
                <Dropdown overlay={type} >
                    <a className="ant-dropdown-link" href="">{this.state.type.length ? this.state.type : 'Select school type'}<Icon type="down" /></a>
                </Dropdown>
                <Button style={{'marginRight':'12px'}} onClick={this.onFilterSearch} type='primary'>Apply Filter</Button>
                <Button className='reset' onClick={this.onReset}>Reset</Button>
            </div>

            <div id='list-view'>
              {this.state.data.length ? <ListCard data={this.state.data}/> : <ListCard data={this.state.data}/>}
            </div>
        </div>
        );
    }
}